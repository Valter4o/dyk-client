import { createRouter, createWebHistory } from "vue-router"
import home from "@/pages/Home.vue"
import explore from "@/pages/Explore.vue"
import login from "@/pages/Login.vue"
import register from "@/pages/Register.vue"
import playground from "@/pages/Playground/index.vue"

export default createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: home },
    { path: "/explore/:gameId", component: playground },
    { path: "/explore", component: explore },
    { path: "/login", component: login },
    { path: "/register", component: register },
  ],
})
