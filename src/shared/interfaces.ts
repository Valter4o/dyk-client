import { Route, Router } from "vue-router"

export interface AppContext {
  store: any
  router: Router
  route: Route
}
