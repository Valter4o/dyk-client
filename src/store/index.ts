import { reactive, readonly, computed } from "vue"

const interaction = reactive({
  isProccesing: false,
})
function toggleProccesing(proccessingState: boolean) {
  interaction.isProccesing = proccessingState
}

const user = reactive({
  token: "",
  username: "",
  email: "",
})

function setUserData(token: string, username: string, email: string) {
  localStorage.setItem("user", JSON.stringify({ token, username, email }))
  user.token = token
  user.username = username
  user.email = email
}

function getUserData() {
  if (localStorage.getItem("user")) {
    // @ts-ignore
    const { token, username, email } = JSON.parse(localStorage.getItem("user"))
    user.token = token
    user.username = username
    user.email = email
  }
}
const isAuth = computed(() => user.token)
function logout() {
  localStorage.removeItem("user")
  user.token = ""
  user.username = ""
  user.email = ""
}

const state = reactive({
  interaction,
  user,
})

const store = {
  state: readonly(state),
  toggleProccesing,
  setUserData,
  getUserData,
  logout,
  isAuth,
}

export default store
