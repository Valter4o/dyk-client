import { post } from "@/shared/services/request"

const emailRegEx: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
function login(data: any) {
  return post("/auth/login", data)
}

async function register(data: any) {
  return post("/auth/register", data)
}

const fieldsObject: any = {
  username: {
    errorMsg: "Username length must be greater than 1",
    validate: (username: string): boolean => username.length > 1,
  },
  email: {
    errorMsg: "Email isn't valid",
    validate: (email: string): boolean => emailRegEx.test(email),
  },
  password: {
    errorMsg: "Password must be at least 6 characters long",
    validate: (pass: string): boolean => pass.length > 6,
  },
  rePass: {
    errorMsg: "Passwords don't match",
    validate: (pass1: string, pass2: string): boolean => pass1 === pass2,
  },
}

function validateField(
  fieldName: string,
  value: string,
  secondValue?: string,
): any {
  console.log(fieldsObject[fieldName].validate(value, secondValue));
  
  return {
    error: fieldsObject[fieldName].validate(value, secondValue)
      ? false
      : fieldsObject[fieldName].errorMsg,
  }
}

const authComposable = () => {
  return {
    login,
    register,
    validateField,
  }
}

export default authComposable
