import { get } from "@/shared/services/request"

function fetchAllGames() {
  return get("/game")
}

async function fetchGame(id: string) {
  return await get("/game/" + id)
}

const gamesComposable = () => {
  return {
    fetchAllGames,
    fetchGame,
  }
}

export default gamesComposable
